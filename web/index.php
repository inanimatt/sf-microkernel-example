<?php
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

// require Composer's autoloader
require __DIR__.'/../vendor/autoload.php';

class AppKernel extends Kernel
{
    use MicroKernelTrait;

    public function registerBundles()
    {
        return [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
        ];
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache';
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    protected function configureContainer(ContainerBuilder $c, LoaderInterface $loader)
    {
        // PHP equivalent of config.yml
        $c->loadFromExtension('framework', [
            'secret' => 'oogaL13emow4zXUraj2POKsbb2w3p7wE',
        ]);
        $c->loadFromExtension('doctrine', [
            'dbal' => [
                'connections' => [
                    'default' => [
                        'host' => getenv('SYMFONY__DATABASE_HOST'),
                        'dbname' => getenv('SYMFONY__DATABASE_NAME'),
                        'user' => getenv('SYMFONY__DATABASE_USER'),
                        'password' => getenv('SYMFONY__DATABASE_PASSWORD'),
                        'charset' => 'utf8',
                    ],
                ],
            ],
        ]);
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        // kernel is a service that points to this class
        // optional 3rd argument is the route name
        $routes->add('/random/{limit}', 'kernel:randomAction');
    }

    public function randomAction($limit)
    {
        return new JsonResponse([
            'number' => rand(0, $limit)
        ]);
    }
}

$kernel = new AppKernel(getenv('APP_ENV') ?: 'dev', getenv('APP_ENV') === 'dev');
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
